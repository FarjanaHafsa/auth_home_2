<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class FontendController extends Controller
{
    public function shop()
    {
        $product = Product::latest()->paginate('15');

        return view('shop', compact('product'));
    }

    public function product_details($id)
    {
        $product = Product::find($id);
        return view('product_details', compact('product'));
    }
}

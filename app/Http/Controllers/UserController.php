<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
      $this->Authorize('admin');  
    $user=User::all();

    return view('admin.user_list',compact('user'));

    }
    public function edit($id){
        $user=User::find($id);
        return view('admin.profile',compact('user'));
    }
}

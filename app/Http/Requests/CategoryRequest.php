<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        
        if ($this->getMethod() == 'POST') {
            $rules = [

                'name' => 'required|unique:categories|max:25|min:3',
                'description' => 'required',
                
                'products' => 'required',
                'image'      => 'required'
            ];
        }
        if ($this->getMethod() == 'PATCH') {


            $rules = [
                'name' => 'required|unique:categories|max:25|min:3',
                'description' => 'required',
                
                'products' => 'required',
                'image'      => 'required'
            ];
        }
        return $rules;

    }
}

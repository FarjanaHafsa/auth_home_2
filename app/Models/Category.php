<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Product;

class Category extends Model
{
    use HasFactory, Softdeletes;
    protected $fillable = ['name', 'description', 'image', 'products'];

    public function product()
    {
        return $this->hasMany(Product::class);
    }
}

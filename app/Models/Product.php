<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Category;

class Product extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['category_id', 'name', 'info', 'image', 'weight'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}

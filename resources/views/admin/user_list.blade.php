<x-master>


        <!-- Header Area End -->
<div class="table-area ">

    <div class="container-fluid pt-4">
      <div class="row">
        <div class="col-md-6">
          <div class="cat-list-left">
            <h4>User List</h2>
          </div>
        </div>
      </div>
    </div>

        <!-- Table Start  -->
       
    <div class="container-fluid pt-4">
      <table class="table cat-list-img">
        <thead>
          <tr>
            
            <th scope="col">SL</th>
            <th scope="col">Name</th>
            <th scope="col">Role</th>
            
            <th class="cat-action" scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($user as $user)
          <tr>
            
            <td>{{ $loop->iteration}}</td>
            <td>{{ $user->name}}</td>
            <td>{{ $user->role->name}}</td>
            
            <td><a class="btn btn-danger btn-sm"  href="#">remove</a><a class="btn btn-info btn-sm"href="#">Edit</a> <a class="btn btn-primary btn-sm"  href="#">view</a></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
   
        <!-- Table End  -->

</x-master>


<x-master>

<x-error><x-error>
<div class="container">
       
        <div class="col-md-12">
            <div class="card mt-4">
                <div  class="card-header text-white bg-secondary">
                 <h5>Profile Edit</h5> 
                </div>
            <div class="card-body">
                  <form action="#" method="post" enctype="multipart/form-data">
                   @csrf
                   @method('patch')
                   <div class="row">
                    <div class="col">
                      <div class="form-group ">
                      <input type="text" name="name" placeholder="name" id="name"class="form-control" value="">
                     </div>
                   </div>
             
                <div class="col">
                <div class="form-group">
        
            <input type="text" name="description" placeholder="description" class="form-control" value="">
                    
                </div></div></div><br>
                <div class="row">
                    <div class="col">
                      <div class="form-group ">
                      <input type="number" name="products" placeholder="products" id="products"class="form-control" value="">
                     </div>
                   </div>
             
                <div class="col">
                <div class="form-group">
        
            <input type="file" name="image" placeholder="image" class="form-control" value="">
                    
                </div></div></div><br>
                <div class="row">
                <div class="col">
                <div class="form-group mb-2">
                <button type="submit" class="btn btn-block btn-secondary form-control">Edit Category</button>
                </div> </div></div>               
        </form>
    </div>
    </div>
    </div>


</x-master>
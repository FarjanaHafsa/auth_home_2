<x-master>

<div class="table-area ">

        <!-- Table Start  -->
       
    
    <div class="container-fluid mt-5">
      <div class="row">
        <div class="col-md-6">
          <div class="cat-list-left">
            <h2>Category List</h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="cat-list-right">
            <a href="{{route('add')}}">Add Category</a>
            <a href="{{route('category.trash')}}">Trash</a>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid pb-3 px-3">
      <table class="table cat-list-img">
        <thead>
          <tr>
            
            <th scope="col">SL</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Products</th>
            <th scope="col">Image</th>
            <th class="cat-action" scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          
          <tr>
            
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
            <a href="{{ route('category.edit',1) }}" class="btn btn-primary btn-sm" style="display:inline">Edit </a>
                                <form action="{{ route('category.destroy',1) }}" method="post"style="display:inline">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn  btn-danger btn-sm">Delete</button>
                                </form>
          </td>
          </tr>
         
        </tbody>
      </table>
    </div>
    </div-table-area>
    <!-- Table End  -->
</x-master>
<x-master>

<div class="table-area">

        <!-- Table Start  -->
       
    
    <div class="container-fluid mt-5">
      <div class="row">
        <div class="col-md-6">
          <div class="cat-list-left">
            <h2>Category Trash</h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="cat-list-right">
            <a href="#">Category</a>
            
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid pb-3 px-3">
      <table class="table cat-list-img">
        <thead>
          <tr>
            
            <th scope="col">SL</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Products</th>
            <th scope="col">Image</th>
            <th class="cat-action" scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          
          <tr>
            
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><a class="btn btn-danger btn-sm"  href="#">Destroy</a><a class="btn btn-info btn-sm"href="#">Restore</a> </td>
          </tr>
         
        </tbody>
      </table>
    </div>
    </div-table-area>
    <!-- Table End  -->
</x-master>
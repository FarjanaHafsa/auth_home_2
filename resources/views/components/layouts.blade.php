<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('fontend/css/style.css')}}">
    <title>warriors</title>
  </head>
  <body>
    <!-- Header Area Start -->
    <heade class="header-area sticky-top">
        <div class="header-top bg-success">
            <div class="container text-uppercase py-1">
                <p>welcome to warriors</p>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-white py-2">
            <div class="container bg-white">
              <div class="logo">
                <a class="navbar-brand text-uppercase" href="#">WARRIORS</a>
              </div>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="nav-colr collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Products</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="#" >About</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link " href="#" >Contact</a>
                  </li>
                </ul>
              </div>
              <div class="header-icon">
                <ul>
                  <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa-brands fa-square-youtube"></i></a></li>
                  <li><a href="#"><i class="fa-brands fa-square-instagram"></i></a></li>
                  <li><a href="#"><i class="fa-brands fa-square-twitter"></i></a></li>
                </ul>
              </div>
            </div>
          </nav>
          <div class="header-bottom py-3 bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header-left">
                            <!-- <a href="#">All CATEGORIES</a> -->
                            <ul>
                              <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                  All CATEGORIES
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                  @foreach( $categories as $category)
                                  <li><a class="dropdown-item">{{ $category->name }}</a></li>
                                 @endforeach
                                </ul>
                              </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="header-search">
                            <form class="d-flex">
                                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-outline-success" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="header-right">
                            <a href="#">Login</a>
                            <a href="#">Register</a>
                            <a href="#">Cart</a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </heade>
    <!-- Header Area End -->
     {{$slot}}
    <!-- Footer Area Start -->
    <footer class="footer-area">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="footer-col-1">
                            <h3>WARRIORS</h3>
                            <p>Contrary to popular belief, Lorem Ipsum is nosimply random text. It has roots in a piece of classical Latin literature from 45 BC, 
                              making it over Lorem Ipsum is nosimply random text.</p>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="footer-col-2">
                            <h3>Information</h3>
                            <ul>
                                <li><a href="">About</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Team Member</a></li>
                                <li><a href="#">Potfolio</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="footer-col-2">
                            <h3>Categories</h3>
                            <ul>
                              @foreach($categories as $category)
                                <li><a href="#">{{$category->name}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="footer-col-2">
                            <h3>Contact Us</h3>
                            <ul>
                                <li><p>Address: Mirpur 12</p></li>
                                <li><p>Phone: +8801756645566</p></li>
                                <li><p>Email: azazahmed623@gmail.com</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom d-flex">
            <div class="container">
                <p>WARRIORS @ 2022</p>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>

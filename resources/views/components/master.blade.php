<html>
    
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('admin/css/style.css')}}">
    
   
   
    <!-- Custom CSS -->
    
    <title>Warriors</title>
  </head>
    <body>
            <!-- Sidebar Area Start -->
    <!-- <input type="checkbox" id="nav-toggle"> -->
    <div class="sidebar-area">
      <div class="sidebar-logo">
        <h1><a href="index.html"> <span>{{ Auth::user()->role->name }}</span></a></h1>
      </div>
      <div class="sidebar-menu">
        <ul>
          <li><a class="acitve" href="index.html"><i class="fa-solid fa-table"></i> <span>Dashbord</span></a></li>
          @can('admin')
          <li><a href="{{route('user.list')}}"><i class="fa-solid fa-table"></i> <span>Customer</span></a></li>
          @endcan
          <li><a href="{{route('category')}}"><i class="fa-solid fa-table"></i> <span>Category</span></a></li>
          <li><a href="#"><i class="fa-solid fa-table"></i> <span>Product</span></a></li>
          <li><a href="{{ route('profile',Auth::user()->id)}}"><i class="fa-solid fa-table"></i> <span>Profile</span></a></li>
          <li><a href="#"><i class="fa-solid fa-table"></i> <span>Trash</span></a></li>
          
        </ul>
      </div>
    </div>
    <!-- Sidebar Area End -->

    <!-- Main Content Area Start -->
    <div class="main-content-area">

        <!-- Header Area Star -->
        <header class="header-area sticky-top">
          <div class="row container-fluid">
            <div class="col-md-4">
              <div class="header-logo">
                <h1>
                  <label for="nav-toggle">
                    <i class="fa-solid fa-bars"></i>
                  </label>
                  Dashbord</h1>
              </div>
            </div>
            <div class="col-md-4">
              <div class="header-search">
                <form class="d-flex form-control me-2">
                  <button type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
                  <input class="" type="search" placeholder="Search hear" aria-label="Search">
                </form>
              </div>
            </div>
            <div class="col-md-4">
            <div class="user-Wrapper">
                <a href="profile.html"><img src="{{asset('admin/images/female-1.png')}}" width="40px" height="40px" alt="azaz-ahmed"></a>
                <div class="user-content">
                  <h3>{{ Auth::user()->name }}</h3>
                <!--logout----> 
                  <form method="POST" action="{{ route('logout') }}">
                  @csrf
                <a class="nav-link px-3" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    this.closest('form').submit();"><i class="fa-solid fa-right-from-bracket"></i></a>
                    </form>
                    <!--logoutend-->
                </div>
              </div>
            
              </div>
              
            </div>
        
        </header>
        <!-- Header Area End -->
        {{$slot}}
        <!-- Dashbord Area Start -->
        
       
    </div>
    <!-- Main Content Area End -->
    </body>
</html>
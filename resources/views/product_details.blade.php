<x-layouts>

    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image:url('/public/fontend/images/page-banner.jpg')">
        <div class="container">
            <div class="row">

                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>PRODUCT DETAILS</h1>
                </div><!-- Page Title End -->

            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- Product Section Start-->
    <div class="product-section section py-4 pb-90">
        <div class="container">

            <!-- Product Wrapper Start-->
            <div class="row">

                <!-- Product Image & Thumbnail Start -->
                <div class="col-lg-7 col-12 mb-30">
                    <!-- Product Image -->
                    <div class="single-product-image product-image-slider fix">
                        <div class="single-image"><img src="{{asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" alt=""></div>
                    </div>

                </div><!-- Product Image & Thumbnail End -->

                <!-- Product Content Start -->
                <div class="single-product-content col-lg-5 col-12 mb-30">

                    <!-- Title -->
                    <h1 class="title">{{$product->name}}</h1>

                    <!-- Product Rating -->
                    <span class="product-rating">
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star"></i>
                        <i class="fa-solid fa-star-half-stroke"></i>
                    </span>


                    <!-- Description -->
                    <div class="description">
                        <p>There are many variations of passages of Lorem Ipsum avaable,majority have suffered alteration in some form, by injected humour, or rdomised words which don't look even slightly believable.</p>
                    </div>

                    <!-- Price -->
                    <span class="product-price">{{$product->price}} Tk</span>

                    <!-- Quantity & Cart Button -->
                    <div class="product-quantity-cart fix">
                        <div class="product-quantity">
                            <span class="dec qtybtn"><i class="fa-solid fa-angle-left"></i></span>
                            <input type="number" value="0" name="qtybox">
                            <span class="inc qtybtn"><i class="fa-solid fa-angle-right"></i></span>
                        </div>
                        <button class="add-to-cart">add to cart</button>
                    </div>

                    <!-- Action Button -->
                    <div class="product-action-button fix">
                        <ul>
                            <li><span>SKU:</span> {{$product->name}}-{{$product->weight}}</li>
                            <li><span>Categories:</span><a href="#">{{$product->category->name}}</a></li>
                            <li><span>Tag:</span><a href="#"> New</a></li>
                            <li><span>Quantity:</span>{{$product->weight}}</li>
                        </ul>
                    </div>

                    <!-- Social Share -->
                    <div class="product-share fix">
                        <h6>Share :</h6>
                        <a href="#"><i class="fa-brands fa-facebook"></i></a>
                        <a href="#"><i class="fa-brands fa-square-youtube"></i></a>
                        <a href="#"><i class="fa-brands fa-square-instagram"></i></a>
                        <a href="#"><i class="fa-brands fa-square-twitter"></i></a>
                    </div>

                </div><!-- Product Content End -->

            </div><!-- Product Wrapper End-->

            <!-- Product Additional Info Start-->
            <div class="row py-5">

                <!-- Nav tabs -->
                <div class="col-12 mt-30">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    DESCRIPTION
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    QUESTIONS ABOUT THIS PRODUCT
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div><!-- Product Additional Info End-->

        </div>
    </div><!-- Product Section End-->

    <!-- Footer Area Start -->
</x-layouts>
<x-layouts>

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">

                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>SHOP PAGE</h1>
                </div><!-- Page Title End -->

            </div>
        </div>
    </div><!-- Page Banner Section End-->
    <!-- Shope Porducts Start  -->
    <div class="container pb-5">
        <div class="shope-row-1">
            <div class="products-items">
                <div class="row">
                    @foreach ($product as $product)

                    <div class="col-md-3">
                        <div class="card" style="width: 18rem;">
                            <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$product->name}}</h5>
                                <p class="card-text">{{$product->price}}Tk</p>
                                <a href="{{ route('product_details',$product->id)}}" class="btn btn-primary">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-layouts>
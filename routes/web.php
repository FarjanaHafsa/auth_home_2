<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FontendController;
use App\Http\Controllers\ProductController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
})->name('dash');
Route::get('/shop', [FontendController::class, 'shop'])->name('shop');
Route::get('/product-details/{id}', [FontendController::class, 'product_details'])->name('product_details');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__ . '/auth.php';

Route::middleware('auth')->group(function () {

    Route::get('/user_list', [UserController::class, 'index'])->name('user.list');

    Route::get(
        '/profile/{id}',
        [UserController::class, 'edit']
    )->name('profile');

    Route::post('/profile/update/{id}', [ProfileController::class, 'update'])->name('profile.update');

    Route::get('/create', function () {
        return view('category.create');
    })->name('c.create');
    Route::get('/edit', function () {
        return view('category.edit');
    })->name('c.edit');

    Route::get('/index', function () {
        return view('c.index');
    })->name('category');
    Route::get('/trash', function () {
        return view('category.trash');
    })->name('category.trash');

    //Route::resource('category', CategoryController::class);
    Route::resource('product', ProductController::class);
});
